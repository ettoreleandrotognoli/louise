import { Event, EventModel } from 'event-timeline/event';


export function createTimelineModel(eventList: Event[]): EventModel[] {
  return eventList
    .map(it => new EventModel(it))
    .sort((a, b) => -(a.date.valueOf() - b.date.valueOf()));
}
