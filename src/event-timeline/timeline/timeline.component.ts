import { Component, Input, OnInit } from '@angular/core';
import { EventModel } from 'event-timeline/event';

@Component({
  selector: 'app-timeline',
  template: `
    <ul>
      <ng-container *ngFor="let event of model">
        <li [ngClass]="{ 'past' : event.isPast()}">
          <app-event [model]="event"></app-event>
        </li>
      </ng-container>
    </ul>
  `,
  styles: [`
    ul {
        list-style-type: none;
        position: relative;
    }
    ul:before {
        content: ' ';
        background: #333333;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }
    ul > li {
        margin: 20px 0;
        padding-left: 20px;
    }
    ul > li:before {
        margin-top: 20px;
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #333333;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }
    ul > li.past:before {
      background: #333333;
    }
  `]
})
export class TimelineComponent implements OnInit {

  @Input()
  public model: EventModel[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
