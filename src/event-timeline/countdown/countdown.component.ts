import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Moment } from 'moment';
import * as moment from 'moment';
import { interval, Subscription } from 'rxjs';
import { HumanizeDurationLanguage, HumanizeDuration } from 'humanize-duration-ts';

@Component({
  selector: 'app-countdown',
  template: `{{countdown}}`,
  styles: [`
  `]
})
export class CountdownComponent implements OnInit, OnDestroy {


  public countdown = '';

  private subscription: Subscription = new Subscription();

  private langService: HumanizeDurationLanguage = new HumanizeDurationLanguage();
  private humanizer: HumanizeDuration = new HumanizeDuration(this.langService);

  @Input()
  public timestamp!: Moment;


  constructor() { }

  ngOnInit(): void {
    this.subscription = interval(100)
      .subscribe((_) => this.refresh());
  }

  public refresh(): void {
    const now = moment();
    const countdown = this.timestamp.diff(now);
    this.countdown = this.humanizer.humanize(countdown, {
      round: true,
    });
  }


  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
