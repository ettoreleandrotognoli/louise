import * as moment from 'moment';
import { Moment } from 'moment';

export interface EventPicture {
  description?: string;
  url: string;
}

export interface Event {
  name?: string;
  date: string;
  pictures?: EventPicture[];
}


export class EventModel {

  public name?: string;
  public date: Moment;
  public pictures: EventPicture[];

  constructor(
    private source: Event
  ) {
    this.name = source.name;
    this.date = moment(source.date);
    this.pictures = source.pictures || [];
  }

  public hasPictures(): boolean {
    return this.pictures.length > 0;
  }

  public isFuture(): boolean {
    return moment().isBefore(this.date);
  }

  public isPast(): boolean {
    return moment().isAfter(this.date);
  }

  public getTimeAgo(): string {
    const deltaTime = this.date.diff(moment());
    return moment.duration(deltaTime).humanize(true);
  }

  public getDate(): string {
    return this.date.format('l');
  }

}
