import { Component, Input, OnInit } from '@angular/core';
import { EventModel } from '.';

@Component({
  selector: 'app-event',
  template: `
    <div class="card text-white bg-dark">
      <div class="row no-gutters">
        <ng-container>
          <div class="col-12 col-sm-4 card-picture">
            <ngb-carousel *ngIf="model.hasPictures()">
              <ng-template *ngFor="let picture of model.pictures"  ngbSlide>
                  <div class="picsum-img-wrapper" [ngStyle]="{'background':'url(' + picture.url + ')'}"></div>
              </ng-template>
            </ngb-carousel>
          </div>
        </ng-container>
        <div class="col">
            <div class="card-body px-2">
              <h6 class="card-title text-right">
                {{model.getTimeAgo()}} - {{model.getDate()}}
              </h6>
              <ng-container *ngIf="model.isFuture(); else pastEvent">
                <h2 class="text-center card-text">
                  <app-countdown [timestamp]="model.date"></app-countdown>
                </h2>
              </ng-container>
              <ng-template #pastEvent>
              </ng-template>
            </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .card-picture .picsum-img-wrapper {
      height: 200px;
      background-size: cover !important;
      background-position: center !important;
    }
    @media(min-width: 768px) {
      .card-picture .picsum-img-wrapper {
        height: 250px;
      }
    }
    @media(min-width: 992px) {
      .card-picture .picsum-img-wrapper {
        height: 300px;
      }
    }
  `]
})
export class EventComponent implements OnInit {

  @Input()
  public model!: EventModel;

  constructor() { }

  ngOnInit(): void {
  }



}
