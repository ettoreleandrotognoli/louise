import { Component, OnInit } from '@angular/core';
import { Event, EventModel } from 'event-timeline/event';
import { createTimelineModel } from 'event-timeline/timeline';

const events: Event[] = [
  {
    date: '2020-05-30',
    pictures: [
      {url: './assets/event/meet.jpg'}
    ]
  },
  {
    date: '2020-06-23',
    pictures: [
      {url: './assets/event/start.jpg'}
    ]
  },
  {
    date: '2020-09-07',
    pictures: [
      {url: './assets/event/date-0.jpg'},
      {url: './assets/event/date-1.jpg'},
      {url: './assets/event/date-2.jpg'},
      {url: './assets/event/date-3.jpg'},
    ]
  },
  {
    date: '2020-09-19',
    pictures: [
      {url: './assets/event/farewell-0.jpg'},
      {url: './assets/event/farewell-1.jpg'},
      {url: './assets/event/farewell-2.jpg'},
      {url: './assets/event/farewell-3.jpg'},
      {url: './assets/event/farewell-4.jpg'},
      {url: './assets/event/farewell-5.jpg'},
    ]
  },
  {
    date: '2021-02-11T06:00:00+0100',
  }
];

@Component({
  selector: 'app-timeline-page',
  template: `
    <div class="container">
      <app-timeline [model]="eventList"></app-timeline>
    </div>
  `,
  styles: [
  ]
})
export class TimelinePageComponent implements OnInit {

  public eventList = createTimelineModel(events);

  constructor() { }

  ngOnInit(): void {
  }

}
